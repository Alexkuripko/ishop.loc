<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%basket}}`.
 */
class m191127_230029_create_basket_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%basket}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'product_id' => $this->integer(),
            'counts' => $this->integer()
        ]);

        $this->createIndex(
            'idx-basket-user_id',
            'basket',
            'user_id'
        );

        $this->addForeignKey(
            'fk-basket-user_id',
            'basket',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );

        $this->createIndex(
            'idx-basket-product_id',
            'basket',
            'product_id'
        );

        // add foreign key for table `user`
        $this->addForeignKey(
            'fk-basket-product_id',
            'basket',
            'product_id',
            'products',
            'id',
            'CASCADE'
        );

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%basket}}');
    }
}
