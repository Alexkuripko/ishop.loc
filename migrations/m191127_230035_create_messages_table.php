<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%messages}}`.
 */
class m191127_230035_create_messages_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%messages}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer(),
            'type_of_problem' => $this->string(),
            'answer' => $this->string()->defaultValue('0'),
            'date' => $this->date()
        ]);

        $this->createIndex(
            'idx-messages-user_id',
            'messages',
            'user_id'
        );

        $this->addForeignKey(
            'fk-messages-user_id',
            'messages',
            'user_id',
            'user',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%messages}}');
    }
}
