<?php
$this->title = 'Регистрация';
use \yii\widgets\ActiveForm;
$form = ActiveForm::begin(['class'=>'form-horizontal']);
?>
<h2>Регистрация</h2>

<?=$form->field($model,'name')->textInput(['autofocus'=> true])?>
<?=$form->field($model,'lastname')->textInput()?>
<?=$form->field($model,'email')->textInput()?>
<?=$form->field($model,'password')->passwordInput()?>

<div>
    <button type="submit" class="btn btn-primary">Регистрация</button>
    <a href="/login">&nbsp уже есть аккаунт</a>
</div>

<?php ActiveForm::end();?>

