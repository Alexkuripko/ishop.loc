<?php
$this->title = 'Авторизация';
use \yii\widgets\ActiveForm;
$form = ActiveForm::begin(['class'=>'form-horizontal']);
?>
    <h2>Вход</h2>

<?=$form->field($model,'email')->textInput(['autofocus'=> true])?>
<?=$form->field($model,'password')->passwordInput()?>

    <div>
        <button type="submit" class="btn btn-primary">Войти</button>
        <a href="/signup" class="btn btn-primary">Регистрация</a>
    </div>


<?php ActiveForm::end();?>