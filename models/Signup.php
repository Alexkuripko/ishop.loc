<?php
/**
 * Created by PhpStorm.
 * Users: alexandr
 * Date: 29.11.19
 * Time: 23:02
 */

namespace app\models;

use yii\base\Model;


class Signup extends Model
{
    public $name;
    public $lastname;
    public $email;
    public $password;

    public function rules()
    {
        return [
          [['name','lastname','email', 'password'],'required'],
            [['name', 'lastname'], 'string', 'min'=>3, 'max' => 12],
            ['email','email'],
            ['email', 'unique', 'targetClass' => 'app\models\User'],
            ['password', 'string','min'=>6,'max'=>12]

        ];
    }

    public function signup()
    {
        $user = new User();
        $user->name = $this->name;
        $user->lastname = $this->lastname;
        $user->email = $this->email;
        $user->setPassword($this->password);

        return $user->save();
    }

}