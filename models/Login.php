<?php

namespace app\models;

use yii\base\Model;

class Login extends Model
{
    public $email;
    public $password;

    public function rules()
    {
        return [
          [['email','password'],'required'],
          ['email','email'],
          ['email','validateEmail'],
            ['password', 'validatePassword']
        ];
    }

    public function validatePassword($attribute)
    {
        $user = $this->getUser();
if($user) {
    if ($user->password != sha1($this->password)) {
        $this->addError($attribute, 'Пароль введен неверно');
    }
}
    }
    public function validateEmail($attribute)
    {
        $user = $this->getUser();
        if(!$user)
        {
            $this->addError( $attribute,'Такого пользователя не существует');
        }
    }

    public function getUser()
    {
        return User::findOne(['email'=>$this->email]);
    }

}